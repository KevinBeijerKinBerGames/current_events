﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour
{
    EnergyLevel energyREf;

	void Start ()
    {
        energyREf = GameObject.FindGameObjectWithTag("Player").GetComponent<EnergyLevel>();
	}
	
    void Update()
    {
        transform.Rotate(0, 0, -1);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            energyREf.IncreaseEnergy(15);

            //Need to disable and re-enable again after a while
            GetComponent<Renderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            StartCoroutine("RespawnEnergyOrbs");
        }
    }

    IEnumerator RespawnEnergyOrbs()
    {
        yield return new WaitForSeconds(20);
        GetComponent<Renderer>().enabled = true;
        GetComponent<CircleCollider2D>().enabled = true;
    }
}
