﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// "AI" for the bullets.
/// </summary>
public class BulletAI : MonoBehaviour
{
    // Stats for the bullet. (can be private if not testing.)
    [SerializeField]
    float speed, lifeTime;

    // Direction of the bullet. Either 1, 0 or -1.
    public float xDirection;
    public float yDirection;

    /// <summary>
    /// Invoke the destroy function when lifetime runs out.
    /// </summary>
    void Start()
    {
        Invoke("Destroy", lifeTime);
    }

    /// <summary>
    /// Move in direction.
    /// </summary>
    void Update ()
	{
        transform.Translate(new Vector2 (xDirection, yDirection) * Time.deltaTime * speed, Space.World);
    }

    /// <summary>
    /// Destroy this bullet.
    /// </summary>
    void Destroy()
    {
        GameObject.Destroy(gameObject);
    }
}