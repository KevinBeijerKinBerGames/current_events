﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// AI for the gunturrets.
/// </summary>
public class GunAI : MonoBehaviour
{
    // Bullet object.
    [SerializeField]
    GameObject bullet;
    // Attached bullet AI script.
    BulletAI bulletAI;

    Animator animator;

    // Spawn point of bullets.
    [SerializeField]
    Transform shotSpawn;

    // Stats for the turret. (Could be private when done testing)
    [SerializeField]
    float fireRate, delay, range;

    GameObject player;

    float distance;
    
    bool isFiring;

	public AudioClip shootSound;

	private AudioSource source;

	private float volHigh = 0.1f;
	private float volLow = 0.05f;

	void Awake()
	{
		source = GetComponent<AudioSource>();
	}



	/// <summary>
	/// Gets player object and sets isFiring to false.
	/// </summary>
	void Start()
    {
        isFiring = false;

        player = GameObject.FindGameObjectWithTag("Player");

        animator = GetComponent<Animator>();

        //offsetX.x = offsetRange;
    }

    /// <summary>
    /// Measure distance and if that's left than the range, start firing, else, stop firing.
    /// </summary>
    void Update()
    {
		distance = Vector3.Distance(gameObject.transform.position, player.transform.position);

        //distance = distance - offsetX.x;

        if (distance < range)
        {
            if (!isFiring)
            {
                isFiring = true;
                InvokeRepeating("Fire", delay, fireRate);
			}
        }
        else
        {
            isFiring = false;
            CancelInvoke("Fire");
        }
    }

    /// <summary>
    /// Fires a bullet.
    /// Determines the direction of movement for the bullet.
    /// </summary>
    void Fire()
    {
		float vol = Random.Range(volLow, volHigh);

		animator.SetTrigger("Shooting");
        GameObject shot = Instantiate(bullet, shotSpawn.position, bullet.transform.rotation);
        shot.transform.SetParent(transform);

        bulletAI = shot.GetComponent<BulletAI>();

		source.PlayOneShot(shootSound, vol);

		if (shotSpawn.position.x < transform.position.x)
        {
            bulletAI.xDirection = -1;
        }
        else if (shotSpawn.position.x == transform.position.x)
        {
            bulletAI.xDirection = 0;
        }
        else if (shotSpawn.position.x > transform.position.x)
        {
            bulletAI.xDirection = 1;
        }

        if (shotSpawn.position.y < transform.position.y)
        {
            bulletAI.yDirection = -1;
        }
        else if (shotSpawn.position.y == transform.position.y)
        {
            bulletAI.yDirection = 0;
        }
        else if (shotSpawn.position.y > transform.position.y)
        {
            bulletAI.yDirection = 1;
        }

        //GetComponent<AudioSource>().Play();
    }
}