﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kills the player by touching him/her.
/// </summary>
public class DeathByContact : MonoBehaviour
{
	public AudioClip hitSound;

	private AudioSource source;

	private float volHigh = 0.1f;
	private float volLow = 0.05f;

	void Awake()
	{
		source = GetComponent<AudioSource>();
	}

	/// <summary>
	/// When the player enters the collider, respawn.
	/// </summary>
	/// <param name="collider"> Object that enters the collider. </param>
	private void OnTriggerEnter2D(Collider2D collider)
    {
		float vol = Random.Range(volLow, volHigh);

		if (collider.tag == "Player")
        {
			source.PlayOneShot(hitSound, vol);
			Respawn respawn = collider.GetComponent<Respawn>();
            respawn.Spawn();
        }
    }
}