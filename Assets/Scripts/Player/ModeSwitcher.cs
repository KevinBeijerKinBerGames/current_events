﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSwitcher : MonoBehaviour {

    Movement movementRef;
    GameObject lowVoltModes;
    GameObject highVoltModes;

    public float highVoltSpeed = 45f;

   

    // Use this for initialization
    void Start () {
        movementRef = GetComponent<Movement>();
        
        
	}
	
	// Update is called once per frame
	void Update () {

      ChangeMode();

    }
    
    public void ChangeMode()
    {
        //Low Volt Mode
      if(movementRef.modes == 0)
        {
  
            movementRef.speed = movementRef.highVoltSpeed;
        }

      //High Volt Mode
      if(movementRef.modes == 1)
        {

            movementRef.speed = highVoltSpeed;


        }
    }
    
}
