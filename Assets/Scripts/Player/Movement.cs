﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public LayerMask IgnoreCollision;
    bool groundJump = false;

	bool airJump = false;

	bool fallJump = false;

	bool madeFallJump = false;

    bool canWallJump = false;

    float jumpForce = 1000f;

    private Transform groundCheck;
    private Transform groundCheck1;
    private Transform wallCheck;
    private Transform wallCheck1;

    public int waitTime = 3;

    bool isGrounded = true;
    bool isGrounded1 = true;
    bool isWalled = false;
    bool isWalled1 = false;

    int nJump = 0;

	private float allJumps = 0.0f;
    private float allDashes = 0.0f;
    public float speed = 10;
    public int modes;
    public float highVoltSpeed;
    Animator animator;

    bool hasJumped = false;

    float jumpTimer = 0.0f;

    public GameObject playRef;
    GameObject[] ignoreCollisions;
    public bool dashed;
    EnergyLevel energyLevelRef;

	public AudioClip jumpSound;

	private AudioSource source;

	private float volHigh = 0.3f;
	private float volLow = 0.2f;

	//Game to be played with a GameCube controller.
	void Awake ()
	{
        animator = gameObject.GetComponent<Animator>();
        groundCheck = transform.Find("groundCheck");
        groundCheck1 = transform.Find("groundCheck1");
        wallCheck = transform.Find("wallCheck");
        wallCheck1 = transform.Find("wallCheck1");
        energyLevelRef = GetComponent<EnergyLevel>();

		source = GetComponent<AudioSource>();
	}

    private void Start()
    {
        highVoltSpeed = speed;
        ignoreCollisions = GameObject.FindGameObjectsWithTag("ColliderIgnore");
    }

    void Update()
    {
        /////////////////////////////////////
        MovingFunction();
        JumpingFunction();
        /////////////////////////////////////
    }

    void FixedUpdate()
	{
		float vol = Random.Range(volLow, volHigh);

		if (isGrounded == false && isGrounded1 == false && groundJump == false && hasJumped == false)
        {
            animator.SetBool("Falling", true);
        }
        else
        {
            animator.SetBool("Falling", false);
        }

        if (groundJump == true)
		{
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
            
			nJump = 1;

			CalculateJumps();

			groundJump = false;

			source.PlayOneShot(jumpSound, vol);
		}

		if (airJump == true)
        {
            if (jumpTimer <= 0.2f)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 0.8f));
            }
            else if (jumpTimer > 0.2f && jumpTimer <= 0.3f)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 1.0f));
            }
            else if (jumpTimer > 0.3f && jumpTimer <= 0.4f)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 1.2f));
            }
            else if (jumpTimer > 0.4f && jumpTimer <= 0.5f)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 1.4f));
            }
            else if (jumpTimer > 0.5f && jumpTimer <= 0.6f)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 1.6f));
            }
            else if (jumpTimer > 0.6f && jumpTimer <= 0.7f)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 1.8f));
            }
			else if (jumpTimer > 0.7f)
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce * 2.1f));
			}

			nJump = 2;

            CalculateJumps();

            airJump = false;

			source.PlayOneShot(jumpSound, vol);
		}

        if (fallJump == true)
		{
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, (jumpForce * 1.8f)));

			madeFallJump = true;

			CalculateJumps();

			fallJump = false;

			source.PlayOneShot(jumpSound, vol);
		}

        if (canWallJump == true)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));

            nJump = 1;

            CalculateJumps();

            canWallJump = false;

			source.PlayOneShot(jumpSound, vol);
		}
    }

    void MovingFunction()
    {
        float move = Input.GetAxis("Horizontal");
        float movement = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        transform.Translate(movement, 0, 0, Space.World);
        SpriteRenderer render = GetComponent<SpriteRenderer>();

        if (movement != 0)
        {
            animator.SetTrigger("PlayerWalk");
        }
        else
        {
            animator.ResetTrigger("PlayerWalk");
        }
        if (movement < 0)
        {
            render.flipX = true;
        }
        if (movement > 0)
        {
            render.flipX = false;
        }

        //transform.Translate(Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime, 0, 0, Space.Self);

        if (Input.GetButtonDown("LeftTrigger") || Input.GetKeyDown(KeyCode.Tab))
        {
            // modes = (modes + 1) % 2;

            if (animator.GetBool("PlayerShift") == true)
            {
                //Yellow, Low Volt Man
                animator.SetBool("PlayerShift", false);
                modes = 0;
            }
            else
            {
                //Pink, High Volt Man
                modes = 1;
                animator.SetBool("PlayerShift", true);
            }
        }

        if ((Input.GetButtonDown("Fire3") || Input.GetKeyDown(KeyCode.LeftShift)) && modes == 1 && dashed == false)
        {
            //This is B button
            animator.SetTrigger("PlayerDash");
            dashed = true;
            Debug.Log("Fire3 is pressed");
            GetComponent<Rigidbody2D>().AddForce(new Vector2(move * 30, 0), ForceMode2D.Impulse);
            CalculateDashes();
            //Reduce Energy level by 10
            energyLevelRef.ReducedEnergy(7);

            for (int i = 0; i < ignoreCollisions.Length; i++)
            {

                Physics2D.IgnoreCollision(playRef.GetComponent<Collider2D>(), ignoreCollisions[i].GetComponent<Collider2D>(), true);

            }
            StartCoroutine("WaitDash");

        }
    }

    void JumpingFunction()
    {
        isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        isGrounded1 = Physics2D.Linecast(transform.position, groundCheck1.position, 1 << LayerMask.NameToLayer("Ground"));
        isWalled = Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Wall"));
        isWalled1 = Physics2D.Linecast(transform.position, wallCheck1.position, 1 << LayerMask.NameToLayer("Wall"));

        if (isGrounded == true || isGrounded1 == true)
        {
            nJump = 0;
            madeFallJump = false;
            hasJumped = false;
            jumpTimer = 0.0f;
            animator.SetBool("PlayerLand", true);
		}

        if (hasJumped == true)
        {
            jumpTimer += Time.deltaTime;
        }

        if ((Input.GetButtonDown("Fire2") || Input.GetKeyDown(KeyCode.Space)) && (isGrounded == true || isGrounded1 == true) && (isWalled == false && isWalled1 == false) && nJump == 0)
        {
            groundJump = true;

            hasJumped = true;
            animator.SetTrigger("PlayerJump");
            animator.SetBool("PlayerLand", false);
		}

        if ((Input.GetButtonDown("Fire2") || Input.GetKeyDown(KeyCode.Space)) && isGrounded == false && isGrounded1 == false && nJump == 1 && modes == 0)
        {
            airJump = true;
            energyLevelRef.ReducedEnergy(3);
		}

        if ((Input.GetButtonDown("Fire2") || Input.GetKeyDown(KeyCode.Space)) && isGrounded == false && isGrounded1 == false && nJump == 0 && madeFallJump == false && hasJumped == false)
        {
            fallJump = true;
            energyLevelRef.ReducedEnergy(3);
		}

        if ((Input.GetButtonDown("Fire2") || Input.GetKeyDown(KeyCode.Space)) && isGrounded == true && isGrounded1 == true && (isWalled == true || isWalled1 == true) & nJump == 0)
        {
            canWallJump = true;

            hasJumped = true;
		}
    }


    //Wait till dash again, Restores the Collision to the game objects
    IEnumerator WaitDash()
    {
        yield return new WaitForSeconds(0.25f);
        energyLevelRef.currentDashTime = 0;
        for (int i = 0; i < ignoreCollisions.Length; i++)
        {
            Physics2D.IgnoreCollision(playRef.GetComponent<Collider2D>(), ignoreCollisions[i].GetComponent<Collider2D>(), false);
        }
        Debug.Log("Fin");
        yield return new WaitForSeconds(waitTime);
        dashed = false;
        Debug.Log("Dashed restored");
    }

    void CalculateDashes()
    {
        allDashes = allDashes + 1;

        GetComponent<DashDisplayer>().DisplayDashes(allDashes);
    }

    void CalculateJumps()
	{
		allJumps = allJumps + 1;

		GetComponent<JumpDisplayer>().DisplayJumps(allJumps);
	}
}
