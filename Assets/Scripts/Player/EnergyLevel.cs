﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyLevel : MonoBehaviour {

    public float maxEnergyLevel = 100;
    public float currentEnergyLevel;
    public Slider energySlider;

    public float maxDashTime;
    public float currentDashTime;
    public Slider dashBar;

	Respawn respawn;

    public bool isDead;

    Movement movementRef;

    public float passiveReductionLow = 0.1f;
    public float passiveReductionHigh = 0.5f;

	public AudioClip deathSound;
	public AudioClip pickupSound;

	private AudioSource source;

	private float volHigh = 0.3f;
	private float volLow = 0.2f;

	private void Awake()
    {
        movementRef = GetComponent<Movement>();

        currentEnergyLevel = maxEnergyLevel;

        maxDashTime = movementRef.waitTime;
        currentDashTime = maxDashTime;

		respawn = GetComponent<Respawn>();

		source = GetComponent<AudioSource>();
	}

    private void Update()
    {
        if(currentEnergyLevel >= 100)
        {
            currentEnergyLevel = 100;
        }
        if (movementRef.modes == 0)
            currentEnergyLevel -= passiveReductionLow * Time.deltaTime;


        if (movementRef.modes == 1)
            currentEnergyLevel -= passiveReductionHigh * Time.deltaTime;

        energySlider.value = currentEnergyLevel;

        currentDashTime += Time.deltaTime;
        dashBar.value = currentDashTime;

    }


    public void ReducedEnergy(float amount)
    {
        currentEnergyLevel -= amount;

        energySlider.value = currentEnergyLevel;

		if (currentEnergyLevel <= 0)
        {
			source.PlayOneShot(deathSound, 0.4f);
			respawn.Spawn();
        }
    }

    public void IncreaseEnergy(float amount)
    {
        currentEnergyLevel += amount;

        energySlider.value = currentEnergyLevel;

		float vol = Random.Range(volLow, volHigh);
		source.PlayOneShot(pickupSound, vol);
	}
}
