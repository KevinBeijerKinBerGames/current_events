﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Set first spawn location and respawns the player if dead.
/// </summary>
public class Respawn : MonoBehaviour
{
    public Vector3 spawnLocation;

    Movement movement;
    EnergyLevel energy;
    GameObject[] pickups;
    Rigidbody2D rb;

    /// <summary>
    /// Sets spawn location.
    /// </summary>
	void Start ()
	{
        spawnLocation = transform.position;

        pickups = GameObject.FindGameObjectsWithTag("PickUp");
        movement = GetComponent<Movement>();
        energy = GetComponent<EnergyLevel>();
        rb = GetComponent<Rigidbody2D>();
	}

    /// <summary>
    /// Resets the player location to spawn location.
    /// </summary>
    public void Spawn()
    {
        transform.position = spawnLocation;
        foreach (GameObject pickup in pickups)
        {
            pickup.GetComponent<Renderer>().enabled = false;
            pickup.GetComponent<CircleCollider2D>().enabled = false;
        }
        movement.enabled = false;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        Invoke("Reactivate", 0.2f);
    }

    void Reactivate()
    {
        foreach (GameObject pickup in pickups)
        {
            pickup.GetComponent<Renderer>().enabled = true;
            pickup.GetComponent<CircleCollider2D>().enabled = true;
        }
        movement.enabled = true;
        movement.dashed = false;
        rb.constraints = RigidbodyConstraints2D.None;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        energy.currentEnergyLevel = energy.maxEnergyLevel;
    }
}