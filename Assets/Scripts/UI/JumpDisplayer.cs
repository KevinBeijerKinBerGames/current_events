﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpDisplayer : MonoBehaviour
{
	float jumps = 0;

	public Text jumpAmount;

	void Update()
	{
		jumpAmount.text = "Jumps: " + ((int)jumps).ToString();
	}

	public void DisplayJumps(float jumpsNums)
	{
		jumps = jumpsNums;
	}
}