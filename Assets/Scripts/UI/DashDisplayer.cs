﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashDisplayer : MonoBehaviour
{
    float dashes = 0;

    public Text dashAmount;

    void Update()
    {
        dashAmount.text = "Dashes: " + ((int)dashes).ToString();
    }

    public void DisplayDashes(float dashesNums)
    {
        dashes = dashesNums;
    }
}