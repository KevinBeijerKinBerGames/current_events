﻿using UnityEngine;
using System.Collections;

public class MatrixMover : MonoBehaviour
{
	public float scrollSpeed = 2.0f;
	public float tileSizeY = 9.0f;

	private Vector2 startPosition;

	void Start()
	{
		startPosition = transform.position;
	}

	void FixedUpdate()
	{
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeY);
		transform.position = startPosition + Vector2.down * newPosition;
	}
}