﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
	private Transform camDirection;

	private Vector3 camOffset;

	private Vector3 moveVector;
	
	void Start()
	{
		camDirection = GameObject.FindGameObjectWithTag("Player").transform;

		camOffset = transform.position - camDirection.position;
	}
	
	void Update()
	{
		moveVector = camDirection.position + camOffset;

		transform.position = moveVector;
	}
}