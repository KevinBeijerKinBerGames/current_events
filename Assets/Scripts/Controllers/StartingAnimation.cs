﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartingAnimation : MonoBehaviour
{
    public Animator animator;

    [SerializeField]
    float startTime, endTime, loadNext;

    [SerializeField]
    string path;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        Invoke("Starting", startTime);
    }

    void Starting()
    {
        animator.SetBool("Start", true);
        Invoke("Ending", endTime);
    }

    void Ending()
    {
        animator.SetBool("Start", false);
        Invoke("Load", loadNext);
    }

    void Load()
    {
		SceneManager.LoadScene(path);
    }
}