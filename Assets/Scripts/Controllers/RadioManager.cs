﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RadioManager : MonoBehaviour
{
	public AudioSource Music;

	void Awake()
	{
		//Allows the music to play through multiple scenes.
		//Credits to Niels.
		GameObject[] musicOjects = GameObject.FindGameObjectsWithTag("Music");
		if (musicOjects.Length > 1)
		{
			Destroy(this.gameObject);
		}
		DontDestroyOnLoad(this.gameObject);
	}

	void Update()
	{
		
		//Stops the music if the at specified Scenes.
		if (SceneManager.GetActiveScene().name == "EndScene")
		{
			Destroy(this.gameObject);
		}
		
	}

	public void ChangeMusic(AudioClip music)
	{
		if (Music.clip.name == music.name)
		{
			return;
		}

		Music.Stop();
		Music.clip = music;
		Music.Play();
		Music.volume = 0.033f;
	}
}