﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour
{
	public AudioClip buttonSound;

	private AudioSource source;

	float switchDelay = 0.0f;

	string sceneName;

	bool hasPressed = false;

	int specifiedFunction = 0;

	public AudioClip nextTrack0;
	public AudioClip nextTrack1;
	public AudioClip nextTrack2;
	public AudioClip nextTrack3;
	public AudioClip nextTrack4;
	public AudioClip nextTrack5;
	public AudioClip nextTrack6;

	bool canPlay0 = false;
	bool canPlay1 = true;
	bool canPlay2 = false;
	bool canPlay3 = false;
	bool canPlay4 = false;
	bool canPlay5 = false;
	bool canPlay6 = false;

	private RadioManager radMan;

	void Awake()
	{
		source = GetComponent<AudioSource>();
	}

	void Start()
	{
		radMan = FindObjectOfType<RadioManager>();
	}

	void Update()
	{
  
	}

	public void MenuButton()
	{
		source.PlayOneShot(buttonSound, 1F);
		sceneName = "Menu";
		specifiedFunction = 1;
		StartCoroutine("Delay");
	}

	public void StartButton()
	{
		source.PlayOneShot(buttonSound, 1F);
		sceneName = "Game";
		specifiedFunction = 1;
		StartCoroutine("Delay");
	}

	public void InstructionsButton()
	{
		source.PlayOneShot(buttonSound, 1F);
		sceneName = "Instructions";
		specifiedFunction = 1;
		StartCoroutine("Delay");
	}

	public void CreditsButton()
	{
		source.PlayOneShot(buttonSound, 1F);
		sceneName = "Credits";
		specifiedFunction = 1;
		StartCoroutine("Delay");
	}

	public void QuitButton()
	{
		source.PlayOneShot(buttonSound, 1F);
		specifiedFunction = 2;
		StartCoroutine("Delay");
	}

	public void MusicChange()
	{
		if (nextTrack1 != null && canPlay1 == true && canPlay2 == false)
		{
			canPlay0 = false;

			radMan.ChangeMusic(nextTrack1);

			canPlay2 = true;
		}

		else if (nextTrack2 != null && canPlay2 == true && canPlay3 == false)
		{
			canPlay1 = false;

			radMan.ChangeMusic(nextTrack2);

			canPlay3 = true;
		}

		else if (nextTrack3 != null && canPlay3 == true && canPlay4 == false)
		{
			canPlay2 = false;

			radMan.ChangeMusic(nextTrack3);

			canPlay4 = true;
		}

		else if (nextTrack4 != null && canPlay4 == true && canPlay5 == false)
		{
			canPlay3 = false;

			radMan.ChangeMusic(nextTrack4);

			canPlay5 = true;
		}

		else if (nextTrack5 != null && canPlay5 == true && canPlay6 == false)
		{
			canPlay4 = false;

			radMan.ChangeMusic(nextTrack5);

			canPlay6 = true;
		}

		else if (nextTrack6 != null && canPlay6 == true && canPlay0 == false)
		{
			canPlay5 = false;

			radMan.ChangeMusic(nextTrack6);

			canPlay0 = true;
		}
		else if (nextTrack0 != null && canPlay0 == true && canPlay1 == false)
		{
			canPlay6 = false;

			radMan.ChangeMusic(nextTrack0);

			canPlay1 = true;
		}
	}

	//Coroutine to delay the switching of scenes to allow the button audio to play completely.
	IEnumerator Delay()
	{
		while (true)
		{
			switchDelay += Time.deltaTime;

			if (switchDelay >= 0.1f)
			{
				hasPressed = true;

				if (specifiedFunction == 1)
				{
					SceneManager.LoadScene(sceneName + "Scene");
				}

				if (specifiedFunction == 2)
				{
					Application.Quit();
				}
			}

			if (hasPressed == true)
			{
				StopAllCoroutines();
			}

			yield return new WaitForEndOfFrame();
		}
	}
}