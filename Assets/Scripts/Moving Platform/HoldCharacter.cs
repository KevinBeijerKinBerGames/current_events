﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that ties the player to the moving platform.
/// </summary>
public class HoldCharacter : MonoBehaviour
{
    /// <summary>
    /// When entering the platform, make the entering object a child of the platform collider.
    /// But only if that object isn't the ground.
    /// </summary>
    /// <param name="collider"> Entering object. </param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != "ground")
        {
            collider.transform.parent = gameObject.transform;
        }
    }

    /// <summary>
    /// When leaving the platform, let the child go.
    /// But only if that object isn't the ground.
    /// </summary>
    /// <param name="collider"> Leaving object. </param>
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag != "ground")
        {
            collider.transform.parent = null;
        }
    }
}