﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for how the checkpoints work.
/// </summary>
public class Checkpoint : MonoBehaviour
{
    // Is this checkpoint activated.
    bool activated;

    // Is this checkpoint's spawn set.
    bool set;

    // Respawn script of the player.
    Respawn respawn;

    // Materials for active (yellow) and deactive (purple).
    [SerializeField]
    Sprite active, deactive;

    GameObject[] pickups;
    EnergyLevel energy;

	public AudioClip checkpointSound;

	private AudioSource source;

	private float volHigh = 0.1f;
	private float volLow = 0.05f;

	void Awake()
	{
		source = GetComponent<AudioSource>();
	}

	/// <summary>
	/// Sets activation and set to false, grabs the player object including its Respawn script.
	/// </summary>
	void Start ()
	{
        activated = false;
        set = false;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        respawn = player.GetComponent<Respawn>();

        pickups = GameObject.FindGameObjectsWithTag("PickUp");
        energy = player.GetComponent<EnergyLevel>();

    }

    /// <summary>
    /// Checks if activated, if so, set spawnlocation of player.
    /// </summary>
    void Update()
    {
		float vol = Random.Range(volLow, volHigh);

		if (activated)
        {
            SpriteRenderer render = GetComponent<SpriteRenderer>();
            render.sprite = active;
            if (!set)
            {
                tag = "Checkpoint";
                respawn.spawnLocation = transform.position;
                respawn.spawnLocation.z = 0;
                energy.currentEnergyLevel = energy.maxEnergyLevel;

                set = true;

				source.PlayOneShot(checkpointSound, vol);
			}
        }
    }

    /// <summary>
    /// If a player enters the checkpoint, activate this checkpoint and deactivate the last one.
    /// </summary>
    /// <param name="collider"> Object that is entering the checkpoint. </param>
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!activated)
        {
            foreach (GameObject pickup in pickups)
            {
                pickup.GetComponent<Renderer>().enabled = false;
                pickup.GetComponent<CircleCollider2D>().enabled = false;
                pickup.GetComponent<Renderer>().enabled = true;
                pickup.GetComponent<CircleCollider2D>().enabled = true;
            }

            GameObject checkpoint = GameObject.FindGameObjectWithTag("Checkpoint");
            if (checkpoint != null)
            {
                checkpoint.tag = "Untagged";
                Checkpoint check = checkpoint.GetComponent<Checkpoint>();
                check.set = false;
                check.activated = false;
                SpriteRenderer render = checkpoint.GetComponent<SpriteRenderer>();
                render.sprite = check.deactive;
            }

            activated = true;
        }
    }
}